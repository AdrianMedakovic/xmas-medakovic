## Weihnachtsgeschichte - Der einsame Santa"
Eines Tages lief der alte Weihnachtsmann am Nordpol herum und ihm wurde klar, dass er etwas ändern muss. Ein Gedankenblitz erweckte sein Gehirn, welcher ihm klar machte, dass die heutige Technik unter anderem mehr, als nur qualifiziertes Personal bräuchte, sodass er sich entschied mehr Geschenke an die Menschen zu bringen, welche auch etwas mit Software beziehungsweise Technik zu tun haben. 

Dem alten Santa war jedoch bewusst, dass er sich mit dem Thema ebenfalls beschäftigen müsste. Der berüchtigte Weihnachtsmann informierte sich Monate lang über diverse Gebiete wie beispielsweise Programmierung, Java als auch die Testung von Programmen. Aufgefallen hierbei ist ihm die Webseite namens "Project Euler". Von dieser Internetseite bearbeitete er mehrere Probleme, sodass er sich bald schon zum Profi entwickelte. Nach einer gewissen Zeit war er so gut, dass er endlich die Wichtigkeit der Programmierung verstand und die jeweiligen Vorteile aufgrund dieser für sich herausschließen konnte. Dieser geistige Segen brachte ihn dazu vier verschiedene Probleme in die Welt zu veröffentlichen, welche die Menschheit motivieren sollte sich nützliches Wissen anzueignen. Jene Person, welches dieses Wissen in der heutigen Zeit noch anwenden kann, bekommt in einem gewissen Fach namens "SEN" eine sehr gute Note.

Seit diesem gedanklichen Durchbruch bekommt die ganze Welt wunderschöne Geschenke im Bereich der Programmierung und Softwaretechnik, was in gewissen Bereichen der Wirtschaft eine Ankurblung und Wissensexplosion auslöste.






