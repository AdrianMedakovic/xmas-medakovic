package Tasks;

public class XMASInputException extends Exception {

    public XMASInputException() {
        super("False input parameter!");
    }

    public XMASInputException(String message) {
        super(message);
    }
}
