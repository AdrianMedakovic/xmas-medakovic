package Tasks;

public class Problem2 {

    private static final String MAX_LIMIT = "4000000";

    public static int solveProblem2(String limit) throws XMASInputException {

        try {

            int n1 = 0;
            int n2 = 1;
            int sum = 0;

            int l_1 = Integer.parseInt(limit);
            while (n2 < l_1) {
                int result = n1 + n2;
                if (result % 2 == 0)
                    sum += result;

                n1 = n2;
                n2 = result;

            }
            return sum;

        } catch (NumberFormatException e) {

            throw new XMASInputException();
        }


    }

    public static void main(String[] args) throws XMASInputException {

        System.out.println(solveProblem2(MAX_LIMIT)); //Output: 4613732
    }

}