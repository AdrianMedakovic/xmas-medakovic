package Tasks;

import java.util.ArrayList;
import java.util.List;

public class Problem3 extends Exception {

    private static final String MAX_LIMIT = "600851475143";

    public static long solveProblem3(String limit) throws XMASInputException {


        try {

            List<Long> factors = new ArrayList<>();

            long l_1 = Long.parseLong(limit);

            for (long i = 2; i <= l_1; i++) {

                while (l_1 % i == 0) {
                    factors.add(i);

                    l_1 = l_1 / i;

                }

            }

            return factors.get(factors.size() - 1);

        } catch (NumberFormatException e) {

            throw new XMASInputException();
        }


    }

    public static void main(String[] args) throws XMASInputException {

        System.out.println(solveProblem3(MAX_LIMIT)); //Output: 6857
    }
}
