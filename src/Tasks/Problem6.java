package Tasks;

public class Problem6 {

    private static final String MAX_LIMIT = "100";

    public static int solveProblem6SquareOfSum(String limit) throws XMASInputException {

        try {

            int l_1 = Integer.parseInt(limit);


            int sum = (l_1 * (l_1 + 1)) / 2;
            return sum * sum;

        } catch (NumberFormatException e) {
            throw new XMASInputException();

        }

    }

    public static int solveProblem6SumOfSquares(String limit) throws XMASInputException {

        int sum = 0;

        try {

            int l_1 = Integer.parseInt(limit);
            for (int i = 1; i <= l_1; i++) {
                sum += i * i;
            }
            return sum;
        } catch (NumberFormatException e) {
            throw new XMASInputException();
        }


    }

    public static int solveProblem6Difference(String limit) throws XMASInputException {

        try {
            int l_1 = Integer.parseInt(limit);

            int difference = solveProblem6SquareOfSum(MAX_LIMIT) -
                    solveProblem6SumOfSquares(MAX_LIMIT);

            return difference;

        } catch (NumberFormatException e) {
            throw new XMASInputException();
        }

    }

    public static void main(String[] args) throws XMASInputException {


        System.out.println(solveProblem6Difference(MAX_LIMIT)); //Output: 25164150

    }

}