package Tasks;


public class Problem1 {

    private static final String MAX_LIMIT = "1000";

    public static int solveProblem1(String limit) throws XMASInputException {

        try {

            int sum = 0;
            int l_1 = Integer.parseInt(limit);
            for (int i = 3; i < l_1; i++) {
                if (i % 3 == 0 || i % 5 == 0) {
                    sum += i;
                }
            }

            return sum;

        } catch (NumberFormatException e) {
            throw new XMASInputException();
        }


    }

    public static void main(String[] args) throws XMASInputException {

        System.out.println(solveProblem1(MAX_LIMIT)); //Output: 233168
    }
}






