package Tasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Problem3Test {

    @Test
    void solveProblem3_1() throws XMASInputException {

        long input = Problem3.solveProblem3("600851475143");
        assertEquals(6857, input);
    }

    @Test
    void solveProblem3_2() throws XMASInputException {

        long input = Problem3.solveProblem3("sadasdh");
        assertEquals(6857, input);
    }
}