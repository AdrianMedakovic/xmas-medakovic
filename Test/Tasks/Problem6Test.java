package Tasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Problem6Test {

    @Test
    void solveProblem6_1Difference() throws XMASInputException {

        int input = Problem6.solveProblem6Difference("100");
        assertEquals(25164150, input);
    }

    @Test
    void solveProblem6Difference_2() throws XMASInputException {

        int input = Problem6.solveProblem6Difference("rrrr");
        assertEquals(25164150, input);
    }
}