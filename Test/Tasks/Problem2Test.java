package Tasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Problem2Test {

    @Test
    void solveProblem2_1() throws XMASInputException {

        int input = Problem2.solveProblem2("4000000");
        assertEquals(4613732, input);
    }


    @Test
    void solveProblem2_2() throws XMASInputException {

        int input = Problem2.solveProblem2("dsfdsfds");
        assertEquals(4613732, input);
    }


}