package Tasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Problem1Test {

    @Test
    void solveProblem1_1() throws XMASInputException {

        int input = Problem1.solveProblem1("1000");
        assertEquals(233168, input);
    }


    @Test
    void solveProblem1_2() throws XMASInputException {


        int input = Problem1.solveProblem1("hello");
        assertEquals(233168, input);
    }
}